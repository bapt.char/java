# java

Learning Java

### Compile
```sh
javac -d class -cp class src/entry.java
```

### Run
```sh
java -cp class entry
```
