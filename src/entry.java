

import java.io.*;


import javax.swing.SwingUtilities;
import javax.swing.JFrame;

public class entry
{
    public static void exercice_3()
    {
      final int NFOIS = 5;

      System.out.println("Bonjour:");
      System.out.println("Programme de calcul de \""+ NFOIS +"\" racines carrées");

      for(int i = 0; i < NFOIS ; i++)
      {
        System.out.print("=> Donnez un nombre : ");
        double x  = keyboard.lireDouble();

        if (x < 0.0)
        {
            System.out.println(x + " ne possède pas de racine carrée!");
        }
        else
        {
          double racx = Math.sqrt(x);
          System.out.println(x +": a pour racine carrée : "+ racx);
        }
      }
      System.out.println("Travail terminé - Au revoir!");
    }

    public static void exercice_4()
    {
      System.out.println("** CALCUL de nombres à la puissance 10 **");
      System.out.print("==> Combien siostudentde nombres voulez vous saisir [0 pour quitter ] : ");

      int count  = keyboard.lireInt();

      for(int i = 0; i < count; i++)
      {
        System.out.print("=> Donnez le nombre N°" + i + ": ");
        double x  = keyboard.lireDouble();
        System.out.println(x + "^10 = " + (Math.pow(x, 10)));
      }

      System.out.println("Travail terminé - Au revoir!");
    }

    public static void exercice_5()
    {
      System.out.print ("=> Saisissez un nombre entier : ") ;
      int n = keyboard.lireInt() ;

      if ((n & 1) == 1)
      {
        System.out.println(n + " est impair");
      }
      else
      {
         System.out.println(n + " est pair");
      }
    }

    public static void exercice_7()
    {
      System.out.print("=> Nombre d’élèves à noter : ");
      int nbEl = keyboard.lireInt();

      //création d'un nouveu tableau de variables double et de taille nbEl
      double notes[] = new double[nbEl] ;

      for (int i = 0 ; i < nbEl ; i++)
      {
        System.out.print ("==> Note de l'élève N°" + (i+1) + " : " ) ;
        notes[i] = keyboard.lireDouble();
      }

      //on additionne tout les nombres
      double somme = 0;
      for (int i = 0; i < nbEl; i++)
        somme += notes[i];

      double moyenne = somme / nbEl;
      System.out.println("\n=> Moyenne de la classe " + moyenne) ;

      int nbElSupMoy = 0;
      for (int i = 0; i < nbEl; i++)
      {
          if (notes[i] > moyenne)
          {
              nbElSupMoy++;
          }
      }

      System.out.println("=> " + nbElSupMoy + " éleves ont plus de la moyenne") ;
    }

    public static void exervice_8()
    {
      Point[] p = new Point[3];

      p[0] = new Point(1, 2);
      p[1] = new Point(4, 5);
      p[2] = new Point(8, 9);

      for (int i = 0; i < p.length; i++)
      {
        p[i].affiche();
      }
    }

    public static void main(String[] args)
    {
      SwingUtilities.invokeLater(new Runnable()
      {
          public void run()
          {
            System.out.println("Created GUI on EDT? " + SwingUtilities.isEventDispatchThread());

            JFrame f = new JFrame("Swing Paint Demo");
            f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
            f.add(new panel());
            f.pack();
            f.setVisible(true);
          }
      });
    }
}

// public class entry
// {
//   public static class utils
//   {
//       public static void println(String str)
//       {
//           System.out.println(str);
//       }
//   }
//
//   public static void main(String[] args)
//   {
//       utils.println("Hello world!");
//
//
//
//       utils.println("Goodbye!");
//   }
// }
