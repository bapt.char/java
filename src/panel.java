import javax.swing.SwingUtilities;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.BorderFactory;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseMotionListener;
import java.awt.event.MouseMotionAdapter;

class panel extends JPanel
{
    private int squareX = 50;
    private int squareY = 50;
    private int squareW = 20;
    private int squareH = 20;

    public panel()
    {
        //setBorder(BorderFactory.createLineBorder(Color.black));
    }

    public Dimension getPreferredSize()
    {
        return new Dimension(250, 200);
    }

    protected void paintComponent(Graphics g)
    {
        super.paintComponent(g);
        g.drawString("Hello world! This is a blue square :D", 25, 25);

        // g.setColor(Color.BLUE);
        // g.fillRect(squareX, squareY, squareW, squareH);
        // g.setColor(Color.BLACK);
        // g.drawRect(squareX, squareY, squareW, squareH);

        g.setColor(Color.BLUE);
        g.fillRect(50, 50, 50, 50);
        g.setColor(Color.BLACK);
        g.drawRect(50, 50, 50, 50);

    }
}
